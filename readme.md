# JPG/PNG image converter to webp.

Examples of command:
```shell
ts-node src/index.ts c images img
ts-node src/index.ts c images/Cat01.jpg img
ts-node src/index.ts c images/Cat01.jpg img/Cat01.webp -q 50
```
