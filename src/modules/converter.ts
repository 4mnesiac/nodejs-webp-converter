import process from 'process';
import path from 'path';
import { existsSync } from 'fs';
import fs from 'fs/promises';
import webp from 'webp-converter';
import { TConverterOptions, TParsedPath } from '../types';

const SOURCE_EXTENSIONS = ['.jpg', '.png'];

const defaultOptions: TConverterOptions = {
  quality: 100,
};

async function getParsedPath(filePath: string): Promise<TParsedPath> {
  const parsedPath = path.parse(filePath);
  return {
    path: path.format({
      ...parsedPath,
      root: parsedPath.root || `${process.cwd()}/`,
    }),
    name: parsedPath.name,
    isFile: !!parsedPath.ext,
  };
}

function getSuccessMsg(length: number) {
  return `Successfully converted ${length} image(-s). Thanks for using our service :)`;
}

/**
 * Convert images from jpg/png to webp
 * @param src Input path of file or folder
 * @param out Output path of file or folder
 * @param options Options
 */
export async function convert2webp(
  src: string,
  dst: string,
  options: TConverterOptions = defaultOptions
): Promise<string[]> {
  const input = await getParsedPath(src);
  const output = await getParsedPath(dst);

  if (process.platform === 'linux') webp.grant_permission();

  let convertedFiles: string[] = [];
  try {
    // Input is a single file
    if (input.isFile) {
      const outputPath: string = output.isFile
        ? output.path
        : path.join(output.path, `${input.name}.webp`);

      if (!output.isFile && !existsSync(output.path)) {
        await fs.mkdir(output.path);
      }

      await webp.cwebp(input.path, outputPath, `-q ${options.quality}`);
      convertedFiles = [outputPath];
    } else {
      // Input is a folder with files
      const outputPath: string = output.isFile
        ? path.dirname(output.path)
        : output.path;

      if (!existsSync(outputPath)) {
        await fs.mkdir(outputPath);
      }

      // Converting multiple files
      const result = [];
      const images = await fs.readdir(input.path);
      for (const img of images) {
        const fileExt = path.extname(img);
        if (SOURCE_EXTENSIONS.includes(fileExt)) {
          const fileName = path.basename(img, path.extname(img));
          const filePath = path.join(input.path, img);
          const output = path.join(outputPath, `${fileName}.webp`);
          await webp.cwebp(filePath, output, `-q ${options.quality}`);
          result.push(filePath);
        }
      }
      convertedFiles = result;
    }
    console.log(getSuccessMsg(convertedFiles.length));
  } catch (error) {
    console.log(error);
    throw error;
  }
  return convertedFiles;
}
