export type TConverterOptions = {
  quality: number;
};

export type TParsedPath = {
  path: string;
  name: string;
  isFile: boolean;
};
