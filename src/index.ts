import { Command, Option } from 'commander';
import convert2webp from './modules';

const program = new Command();

program
  .name('image-converter')
  .description('CLI to convert .png or .jpg files to .webp format.')
  .version('0.1.0');

program
  .command('convert2webp')
  .alias('c')
  .description('Convert png/jpg to webp.')
  .argument('<input>', '<string> Input path of folder or file')
  .argument('<output>', '<string> Output path of folder or file')
  .addOption(
    new Option('-q, --quality', 'Quality of image [1-100]').default(100)
  )
  .action(
    async (src: string, dst: string, options: Record<string, unknown>) => {
      const converterOptions = {
        quality: Number(options.quality),
      };
      await convert2webp(src, dst, converterOptions);
    }
  );

program.parseAsync();
